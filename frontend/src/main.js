import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Welcome from './components/welcome.vue'
import Editor from './components/editor.vue'
import Todos from './components/todos.vue'
import store from './store'

console.log(store)

Vue.config.productionTip = false

/** ----- Routing: ----- */

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Welcome },
    { path: '/Editor', component: Editor },
    { path: '/Todos', component: Todos },
]

const router = new VueRouter({
    routes,
})

/** ----- Rendering: ----- */

new Vue({
    el: '#todoapp',
    router,
    store,
    // computed: mapGetters([
    //   'doneTodos',
    //   'doneTodosCount',
    //   'getTodosById',
    // ]),
    render: h => h(App),
}).$mount('#todoapp')
