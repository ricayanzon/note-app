import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        /** backend runs locally on port 3001 (see app.listen() in app.js) */
        domain: 'http://localhost:3001/',
        todos: [],
    },
    mutations: {
        addTodo(state, todo) {
            state.todos.push(todo)
        },
        deleteTodo(state, id) {
            if (id > -1) { // might not be necessary
                state.todos.splice(id, 1)
            }
        },
        // changeDone(state, id, done) {
        //     this.todos[id].done = done
        // }
    },
    actions: {
        async addTodo({ commit }, todo) {
            try {
                await Vue.http.post(`${this.state.domain}todos`, todo)
            } catch (err) {
                console.log(err)
            }
            commit('addTodo', todo)
        },
        async deleteTodo({ commit }, id) {
            try {
                await Vue.http.delete(`${this.state.domain}todos`, id)
            } catch (err) {
                console.log(err)
            }
            commit('deleteTodo', id)
        },
        // changeDone({ commit }, id, done) {
        //     Vue.http.delete(`${this.state.domain}todos`, id, done)
        //         .then(commit('changeDone', id, done), res => console.log(res.status))
        // }
    },
    getters: {
        // may not be needed as backend calls only happen from here
        // domain() {
        //   return this.state.domain;
        // },
    },
})

export default store
