module.exports = {

  'env': {
      'browser': true,
      'node': true,
      'es6': true,
  },
  'plugins': ['vue'],
  'extends': [
      'eslint:recommended',
      'plugin:vue/recommended',
  ],
  'parserOptions': {
      'parser': "babel-eslint",
      'ecmaVersion': 2018,
      'sourceType': 'module',
  },
  'rules': {
      'indent': ['error', 4, {
          'SwitchCase': 1
      }],
      'vue/html-indent': ['error', 4, {
          'alignAttributesVertically': false
      }],
      'quotes': ['error', 'single'],
      'semi': ['error', 'never'],
      'brace-style': ['error', '1tbs'],
      'camelcase': ['error', {'properties': 'always'}],
      'eol-last': ['error'],
      'no-trailing-spaces': ['error'],
      'arrow-parens': [
          'error',
          'as-needed',
      ],
      'no-var': 'error',
      'curly': 'error',
      'comma-dangle': ['error', 'always-multiline'],
      'vue/max-attributes-per-line': ['warn', {
          'singleline': 3,
          'multiline': {
            'max': 2,
            'allowFirstLine': true
          }
      }],
      'vue/multiline-html-element-content-newline': 'off',
      'vue/html-closing-bracket-newline': ['error', {
          'singleline': 'never',
          'multiline': 'never'
      }],
      'vue/name-property-casing': 'off',
      'vue/singleline-html-element-content-newline': 'off',
      'vue/padding-line-between-blocks': ['warn', 'always'],
      'vue/v-on-function-call': ['error', 'always'],
      'vue/valid-v-slot': 'error',
      'vue/v-slot-style': ['error', 'shorthand'],
      'vue/no-deprecated-slot-attribute': 'error',
      'vue/no-deprecated-slot-scope-attribute': 'error',
      'vue/no-v-html': 'error',
      'no-console': 'warn',
      'prefer-const': 'error',
      'prefer-template': 'warn',
      'object-shorthand': 'warn',
      'no-template-curly-in-string': 'error',
      'no-return-await': 'error',
      'no-else-return': ['error', {
          allowElseIf: false,
      }],
      'eqeqeq': 'error',

      // spacing rules
      'array-bracket-spacing': 'error',
      'arrow-spacing': 'error',
      'block-spacing': 'error',
      'func-call-spacing': 'error',
      'generator-star-spacing': 'error',
      'key-spacing': 'error',
      'keyword-spacing': 'error',
      'object-curly-spacing': ['error', 'always'],
      'space-before-blocks': 'error',
      'space-before-function-paren': ['error', {
          'anonymous': 'never',
          'named': 'never',
          'asyncArrow': 'always'
      }],
      'space-in-parens': 'error',
      'space-infix-ops': 'error',
      'space-unary-ops': 'error',
      'template-curly-spacing': ['error', 'never'],
  },
  'globals': {
      '$': true,
      'jQuery': true,
      'angular': true,
      'Chartist': true,
      'fabric': true,
      'Vue': true,
      'Vuex': true,
      'hljs': true,
      'ga': true,
      'describe': true,
      'it': true,
      'expect': true,
      'mount': true,
      'createLocalVue': true,
      'Calendly': true,
  },

}