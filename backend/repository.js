/* eslint-disable */

const {Sequelize, Op} = require('sequelize')
const sequelize = new Sequelize('postgres://postgres:12345@localhost:5432/postgres')
const {ToDo, Change} = require('./models')

class TodoRepository {

    async logChange(info){
       await Change.create(info)
    }
 
    async addToDo(item){
       const createdItem = await ToDo.create(item)
       return createdItem
    }
 
    async getItem(getId){
       ToDo.hasMany(Change)
       Change.belongsTo(ToDo)
      // For Setup:
      //  await Change.sync({
      //    force: true
      //  })
 
       const item = await ToDo.findByPk(getId,{include: Change})
       return item
    }
 
    async changeToDo(update, getId){
       const item = await this.getItem(getId)
 
       if(item !== null){
 
          await this.logChange({
             ToDoId: getId,
             changedAttribute: {
                //oldTitle: item.title,
                newTitle: update.title,
 
                //oldContent: item.content,
                newContent: update.content,
 
                //oldDone: item.done,
                //newDone: update.done
             }
          })
 
          return await item.update({
             done: update.done,
             title: update.title,
             content: update.content
          })
       }
       return item
    }
 
    async toggleDone(id){
       const item = await this.getItem(id)
 
       if(item !== null){
          
          await this.logChange({
             ToDoId: id,
             changedAttribute: {newDone: !item.done}
          })
 
       return await item.update({
             done: !item.done
          })
       }
       return item
    }
  
    async toggleDelete(getId){
       const item = await this.getItem(getId)
       
       if(item !== null){
          
          await this.logChange({
             ToDoId: getId,
             changedAttribute: {newDeleted: !item.deleted}
          }) 
          await item.update({
             deleted: !item.deleted
          })
       }
       return item
    }
    async average(){
       
       const avg = await sequelize.query(
          `WITH change_list (todoid, change_count) AS ( 
             SELECT "ToDoId", COUNT(*)
             FROM "Changes"
             GROUP BY "ToDoId"
          )
          
          SELECT AVG(COALESCE(change_count,0))
          FROM "ToDos"
          LEFT JOIN change_list ON "ToDos".id = change_list.todoid
          WHERE deleted = false`
          )
       return avg[0]
    }
    createQuery(inputObject){
 
       let query = {deleted: false}
       if(inputObject.id !== undefined && typeof(inputObject.id) === 'number'){
          query.id = inputObject.id
       }
       if(inputObject.title !== undefined && typeof(inputObject.title) === 'string'){
          query.title = inputObject.title
       }
       if(inputObject.done !== undefined && typeof(inputObject.done) === 'boolean'){
          query.done = inputObject.done
       }
       if(inputObject.createdAfter !== undefined /*&& Object.prototype.toString.call(inputObject.createdAfter) === '[object Date]'*/){
          query.createdAt = {[Op.gte] : inputObject.createdAfter}
       }
       if(inputObject.createdBefore !== undefined /*&& Object.prototype.toString.call(inputObject.createdBefore) === '[object Date]'*/){
        query.createdAt = {[Op.lte] : inputObject.createdBefore}
       }
       if(inputObject.deleted !== undefined && typeof(inputObject.deleted) === 'boolean'){
          query.deleted = inputObject.deleted
       }
       return query
    }
    async query(obj, lookingForList = true){
       const query = this.createQuery(obj)
 
       if(lookingForList){
          const list = await ToDo.findAll({
             where: query,
             attributes: {exclude: ['deleted']}
          })
          return list
       }
       const count = await ToDo.count({where: query})
       return {numberOfCorespondingTodos: count}
    }
 }

 module.exports = new TodoRepository()