/* eslint-disable */

const {Sequelize, DataTypes} = require('sequelize')
const sequelize = new Sequelize('postgres://postgres:12345@localhost:5432/postgres')

const ToDo = sequelize.define('ToDo', {
    title:{
      type: DataTypes.STRING,
      allowNull: false
   },
    content:{
        type: DataTypes.TEXT
        //defaultValue:
    },
    done: {
       type: DataTypes.BOOLEAN,
       allowNull: false,
       defaultValue: false
    },
    deleted: {
       type: DataTypes.BOOLEAN,
       allowNull: false,
       defaultValue: false
    }
})

const Change = sequelize.define('Change', {
   changedAttribute: {
      type: DataTypes.JSON,
      allowNull: false
   },
   date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
   }
},
   {
      timestamps:false
   }
)
module.exports ={ToDo, Change}