const Koa = require('koa')
const Router = require('koa-router')
const fs = require('fs')      // html file access
const BP = require('koa-bodyparser') // für text submit
const repository = require('./repository')
const serve = require('koa-static')
const path = require('path')
const { ToDo, Change } = require('./models')

const app = new Koa()
const router = new Router()
const indexPath = path.join(__dirname, '../' ,'dist')
const componentPath = path.join(__dirname, 'components' )

app.use(BP()) // scheinbar pflicht
app.use(serve(indexPath))
app.use(serve(__dirname))
app.use(serve(componentPath))
// For Setup:
// app.use(
//    async () => {
//       await ToDo.sync({
//          force: true
//       }),
//       await Change.sync({
//          force: true
//       })
//    }
// )

router.get('/', async (ctx, next) => {
   ctx.type = 'html'    // notwendig
   ctx.body = fs.createReadStream('../frontend/dist/index.html')
})

router.get('/todos', async ctx => { 
   ctx.body = await repository.query({deleted:false})
   ctx.status = 200
})

router.get('/todos/:id', async ctx => { 
   const id = ctx.params.id
   const item = await repository.getItem(id)
   if(!item || item.deleted){
      ctx.status = 404
      ctx.body = {message: 'address not found'}
      return
   }
   ctx.body = item
})

router.get('/deleted', async ctx => {
   ctx.body = await repository.query({deleted:true})
   ctx.status = 200
})

router.get('/averageChanges', async ctx => {
   ctx.body = await repository.average()
   ctx.status = 200
})

router.get('/amountOf', async ctx => {
   const {done, title, id, createdAfter, createdBefore, deleted} = ctx.query
   ctx.body = await repository.query({done, title, id, createdAfter, createdBefore, deleted}, false)
   ctx.status = 200
})

router.get('/listOf', async ctx => {
   const {done, title, id, createdAfter, createdBefore, deleted} = ctx.query
   ctx.body = await repository.query({done, title, id, createdAfter, createdBefore, deleted})
   ctx.status = 200
})

router.post('/todos',async ctx => {
   const {title, content} = ctx.request.body
   if(!title){
      ctx.body={message: 'invalid title'}
      ctx.status= 400
      return
   }
   
   ctx.body = await repository.addToDo({title, content})
   ctx.status= 200
})

router.put('/todos/:id',async ctx => {
   id = ctx.params.id
   const {title, content} = ctx.request.body
   item = await repository.changeToDo({title, content}, id)
   if(!item || item.deleted){
      ctx.body={message: 'address not found'}
      ctx.status= 400
      return
   }

   ctx.body = item
   ctx.status = 200
})

router.put('/todos/:id/done', async ctx => {
   id = ctx.params.id
   item = await repository.toggleDone(id)
   if(!item || item.deleted){
      ctx.body={message: 'address not found'}
      ctx.status= 400
      return
   }

   ctx.body = item
   ctx.status = 200
})

/* router.put('/todos/:id/restore', async ctx => {
   id = ctx.params.id
   item = await repository.toggleDelete(id)
   if(!item || !item.deleted){
      ctx.body={message: 'address not found'}
      ctx.status= 400
      return
   }

   ctx.body = item
   ctx.status = 200
}) */

router.delete('/todos/:id',async ctx => {
   id = ctx.params.id
   item = await repository.toggleDelete(id)
   if(!item){
      ctx.body={message: 'address not found'}
      ctx.status= 400
      return
   }
   ctx.body = item
   ctx.status = 200
})

app.on('error', function(err){
   console.error(err)
})

app.use(router.routes());

/** changed to 3001 as 3000 is reserved for re-invent backend */
app.listen(3001);